"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function StrongParams(params) {
    return function (request, response, next) {
        var strongParams = {};
        var weakParams = request.body;
        Object.entries(params).forEach(function (_a) {
            var key = _a[0], value = _a[1];
            var weakParam = weakParams[key];
            if (weakParam && typeof weakParam === value) {
                strongParams[key] = weakParam;
            }
        });
        response.locals.strongParams = strongParams;
        request.body = null;
        return next();
    };
}
exports.StrongParams = StrongParams;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3Ryb25nUGFybXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbWlkZGxld2FyZS9TdHJvbmdQYXJtcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLFNBQWdCLFlBQVksQ0FBRSxNQUFnQztJQUMxRCxPQUFPLFVBQUMsT0FBZ0IsRUFBRSxRQUFrQixFQUFFLElBQWtCO1FBRTVELElBQU0sWUFBWSxHQUE0QixFQUFFLENBQUM7UUFDakQsSUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztRQUVoQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEVBQVc7Z0JBQVYsV0FBRyxFQUFDLGFBQUs7WUFFdEMsSUFBTSxTQUFTLEdBQVcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTFDLElBQUcsU0FBUyxJQUFJLE9BQU8sU0FBUyxLQUFLLEtBQUssRUFBQztnQkFFdkMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQzthQUNqQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQzVDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBRXBCLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFFbEIsQ0FBQyxDQUFBO0FBQ0wsQ0FBQztBQXRCRCxvQ0FzQkMifQ==