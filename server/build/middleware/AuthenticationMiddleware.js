"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Session_1 = require("../persistance/Session");
function AuthenticationMiddleware(request, response, next) {
    var _a;
    return __awaiter(this, void 0, void 0, function () {
        var sessionID;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    console.log("entered in auth middleware");
                    if (!((_a = request.signedCookies) === null || _a === void 0 ? void 0 : _a.userCookieName)) return [3 /*break*/, 2];
                    console.log("cookies signed with sid");
                    console.log(request.signedCookies);
                    console.log(request.signedCookies.userCookieName);
                    return [4 /*yield*/, Session_1.Session.findOne({ sid: { $eq: request.signedCookies.userCookieName } })];
                case 1:
                    sessionID = _b.sent();
                    console.log("sessionID found " + sessionID);
                    if (sessionID && request.signedCookies.userCookieName.length == 32) {
                        response.locals.userAuth = true;
                        console.log("session found!!");
                        next();
                    }
                    else {
                        response.locals.userAuth = false;
                        console.log('cookies found but sessionID is wrong');
                        response.sendStatus(400);
                    }
                    return [3 /*break*/, 3];
                case 2:
                    response.sendStatus(400);
                    console.log("not a signed cookie!");
                    //return response.redirect('/login');
                    response.locals.userAuth = false;
                    console.log(response.locals.userAuth);
                    console.log("can not find session, request to login back");
                    _b.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.AuthenticationMiddleware = AuthenticationMiddleware;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aGVudGljYXRpb25NaWRkbGV3YXJlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL21pZGRsZXdhcmUvQXV0aGVudGljYXRpb25NaWRkbGV3YXJlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0Esa0RBQStDO0FBRy9DLFNBQXNCLHdCQUF3QixDQUFFLE9BQWUsRUFBQyxRQUFpQixFQUFDLElBQWlCOzs7Ozs7O29CQUczRixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUE7Z0NBQ3RDLE9BQU8sQ0FBQyxhQUFhLDBDQUFFLGNBQWM7b0JBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQTtvQkFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDL0IscUJBQU0saUJBQU8sQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUMsRUFBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUMsRUFBQyxDQUFDLEVBQUE7O29CQUFyRixTQUFTLEdBQUksU0FBd0U7b0JBQzNGLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQW1CLFNBQVcsQ0FBQyxDQUFDO29CQUM1QyxJQUFHLFNBQVMsSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFDO3dCQUM5RCxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7d0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFDL0IsSUFBSSxFQUFFLENBQUM7cUJBQ1Y7eUJBQUk7d0JBQ0QsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO3dCQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7d0JBQ3BELFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQzVCOzs7b0JBSUQsUUFBUSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNwQyxxQ0FBcUM7b0JBQ3JDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztvQkFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7Ozs7OztDQUV0RTtBQTdCRCw0REE2QkMifQ==