"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_dom_1 = __importDefault(require("react-dom"));
var App_1 = __importDefault(require("./js/App"));
var serviceWorker = __importStar(require("./serviceWorker"));
react_dom_1.default.render(<react_1.default.StrictMode>
    <App_1.default />
  </react_1.default.StrictMode>, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9jbGllbnQvc3JjL2luZGV4LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxnREFBMEI7QUFDMUIsd0RBQWlDO0FBQ2pDLGlEQUEyQjtBQUMzQiw2REFBaUQ7QUFFakQsbUJBQVEsQ0FBQyxNQUFNLENBQ2IsQ0FBQyxlQUFLLENBQUMsVUFBVSxDQUNmO0lBQUEsQ0FBQyxhQUFHLENBQUMsQUFBRCxFQUNOO0VBQUEsRUFBRSxlQUFLLENBQUMsVUFBVSxDQUFDLEVBQ25CLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQ2hDLENBQUM7QUFFRix1RUFBdUU7QUFDdkUsd0VBQXdFO0FBQ3hFLDJEQUEyRDtBQUMzRCxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMifQ==