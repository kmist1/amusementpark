"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var core_1 = require("@material-ui/core");
function App() {
    var _a = react_1.default.useState(), webSocket = _a[0], setWebSocket = _a[1];
    var _b = react_1.default.useState([String]), messages = _b[0], setMessages = _b[1];
    var _c = react_1.default.useState(''), messageToSend = _c[0], setMessageToSend = _c[1];
    react_1.default.useEffect(function () {
        var webSocket = new WebSocket('ws://localhost:5000');
        setWebSocket(webSocket);
        webSocket.onmessage = function (message) {
            console.log('Received message: ', message);
            // Set state calls are batched, this means they are not guaranteed to capture every intermediary change.
            // Passing a function to set state ensures we set states in successive order (we'll talk more about this later)
            setMessages(function (oldMessages) { return __spreadArrays(oldMessages, [message]); });
        };
        return function () { return webSocket.close(); };
    }, []);
    function sendMessage() {
        // Don't bother trying to send a message to a null or closed websocket
        if (webSocket && webSocket.readyState === WebSocket.OPEN) {
            // Send the currently typed message to the websocket
            webSocket.send(messageToSend);
            console.log('Sent message to websocket: ', messageToSend);
        }
    }
    return (<div style={{ height: '100vh', backgroundColor: 'lightgray' }}>
      <h1>Websocket Demo</h1>
      
      

      <core_1.Input value={messageToSend} onChange={function (event) { return setMessageToSend(event.target.value); }} name={'INPUT FOR THE WEBSOCKET'}/>

      <core_1.Button onClick={sendMessage}>
        Send Message
      </core_1.Button>

      {
    // Create H1 components from each message the server sent back
    // DONT EVER SET A LIST KEY AS AN INDEX (I'm only doing it here because we don't have a unique value to key off of)
    messages.map(function (message, index) {
        return (<h1 key={index}>

                {JSON.stringify(message.data)}

              </h1>);
    })}
    </div>);
}
exports.default = App;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vY2xpZW50L3NyYy9qcy9BcHAudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLGdEQUEwQjtBQUMxQiwwQ0FBbUQ7QUFHbkQsU0FBVSxHQUFHO0lBRUwsSUFBQSwrQkFBNEMsRUFBM0MsaUJBQVMsRUFBRSxvQkFBZ0MsQ0FBQztJQUM3QyxJQUFBLHVDQUFrRCxFQUFqRCxnQkFBUSxFQUFFLG1CQUF1QyxDQUFDO0lBQ25ELElBQUEsaUNBQXNELEVBQXJELHFCQUFhLEVBQUUsd0JBQXNDLENBQUM7SUFHN0QsZUFBSyxDQUFDLFNBQVMsQ0FBQztRQUVkLElBQU0sU0FBUyxHQUFJLElBQUksU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFFeEQsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXhCLFNBQVMsQ0FBQyxTQUFTLEdBQUcsVUFBQyxPQUFPO1lBRTVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFM0Msd0dBQXdHO1lBQ3hHLCtHQUErRztZQUMvRyxXQUFXLENBQUMsVUFBQyxXQUFlLElBQUssc0JBQUksV0FBVyxHQUFFLE9BQU8sSUFBeEIsQ0FBeUIsQ0FBQyxDQUFDO1FBRTlELENBQUMsQ0FBQztRQUVGLE9BQU8sY0FBTSxPQUFBLFNBQVMsQ0FBQyxLQUFLLEVBQUUsRUFBakIsQ0FBaUIsQ0FBQztJQUVqQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFUCxTQUFTLFdBQVc7UUFFbEIsc0VBQXNFO1FBQ3RFLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLElBQUksRUFBRTtZQUV4RCxvREFBb0Q7WUFDcEQsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUU5QixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLGFBQWEsQ0FBQyxDQUFDO1NBRTNEO0lBRUgsQ0FBQztJQUVELE9BQU8sQ0FDTCxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFdBQVcsRUFBQyxDQUFDLENBQzNEO01BQUEsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FJdEI7Ozs7TUFBQSxDQUFDLFlBQUssQ0FDRixLQUFLLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FDckIsUUFBUSxDQUFDLENBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFwQyxDQUFvQyxDQUFDLENBQzFELElBQUksQ0FBQyxDQUFDLHlCQUF5QixDQUFDLEVBR3BDOztNQUFBLENBQUMsYUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUMzQjs7TUFDRixFQUFFLGFBQU0sQ0FFUjs7TUFBQTtJQUNFLDhEQUE4RDtJQUM5RCxtSEFBbUg7SUFDbkgsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFDLE9BQVcsRUFBRSxLQUFTO1FBRWxDLE9BQU8sQ0FFSCxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FFYjs7Z0JBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FFL0I7O2NBQUEsRUFBRSxFQUFFLENBQUMsQ0FDUixDQUFBO0lBQ0gsQ0FBQyxDQUFDLENBR047SUFBQSxFQUFFLEdBQUcsQ0FBQyxDQUNQLENBQUM7QUFDSixDQUFDO0FBRUQsa0JBQWUsR0FBRyxDQUFDIn0=