"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var faQsSchema = new mongoose_1.Schema({
    FAQs: {
        type: String,
        required: false,
    },
    HelpClientMessage: {
        type: String,
        required: true,
    }
});
// Export the compiled model
exports.FAQs = mongoose_1.model('FAQs', faQsSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRkFRcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9GQVFzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBSXZDLElBQU0sVUFBVSxHQUFrQixJQUFJLGlCQUFNLENBQVE7SUFFaEQsSUFBSSxFQUFFO1FBQ0YsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsS0FBSztLQUNsQjtJQUVELGlCQUFpQixFQUFFO1FBQ2YsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtLQUNqQjtDQUNKLENBQUMsQ0FBQztBQUdILDRCQUE0QjtBQUNmLFFBQUEsSUFBSSxHQUFHLGdCQUFLLENBQVEsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDIn0=