"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = __importStar(require("mongoose"));
var ticketSchema = new Mongoose.Schema({
    email: {
        type: String,
        required: true,
    },
    startDate: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
            },
            message: 'start date should only contains date formate'
        }
    },
    endDate: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/.test(value);
            },
            message: 'end date should only contains date formate'
        }
    },
    numPeople: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0 && value < 7;
            },
            message: 'Number of people should only contains numbers between 0 to 7'
        }
    },
    whichPark: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 1 && /^(?![\s.]+$)[a-zA-Z\s.]*$/.test(value);
            },
            message: 'Name of the park location should only contains stings'
        }
    },
});
// Export the compiled model
exports.BuyTickets = Mongoose.model('buyTickets', ticketSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQnV5VGlja2V0cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9CdXlUaWNrZXRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLGlEQUFxQztBQUlyQyxJQUFNLFlBQVksR0FBaUMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFjO0lBRWhGLEtBQUssRUFBRTtRQUNILElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7S0FDakI7SUFFRCxTQUFTLEVBQUU7UUFDUCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxnRUFBZ0UsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUcsQ0FBQztZQUVELE9BQU8sRUFBRSw4Q0FBOEM7U0FDMUQ7S0FDSjtJQUVELE9BQU8sRUFBRTtRQUNMLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFFZCxRQUFRLEVBQUU7WUFFTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUU5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGdFQUFnRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1RyxDQUFDO1lBRUQsT0FBTyxFQUFFLDRDQUE0QztTQUN4RDtLQUNKO0lBRUQsU0FBUyxFQUFFO1FBQ1AsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxHQUFHLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7WUFFRCxPQUFPLEVBQUUsOERBQThEO1NBQzFFO0tBQ0o7SUFFRCxTQUFTLEVBQUU7UUFDUCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSwyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkUsQ0FBQztZQUVELE9BQU8sRUFBRSx1REFBdUQ7U0FDbkU7S0FDSjtDQUdKLENBQUMsQ0FBQztBQUtILDRCQUE0QjtBQUNmLFFBQUEsVUFBVSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQWMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDIn0=