"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var sessionSchema = new mongoose_1.Schema({
    ttl: {
        type: Date,
        default: Date.now,
        index: { expires: '5m' }
    },
    sid: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});
// Export the compiled model
exports.Session = mongoose_1.model('session', sessionSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2Vzc2lvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9TZXNzaW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBSXZDLElBQU0sYUFBYSxHQUFxQixJQUFJLGlCQUFNLENBQVc7SUFFekQsR0FBRyxFQUFFO1FBQ0QsSUFBSSxFQUFFLElBQUk7UUFDVixPQUFPLEVBQUUsSUFBSSxDQUFDLEdBQUc7UUFDakIsS0FBSyxFQUFFLEVBQUMsT0FBTyxFQUFFLElBQUksRUFBQztLQUN6QjtJQUNELEdBQUcsRUFBRTtRQUNELElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7S0FDakI7SUFFRCxLQUFLLEVBQUU7UUFDSCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO0tBQ2pCO0NBQ0osQ0FBQyxDQUFDO0FBR0gsNEJBQTRCO0FBQ2YsUUFBQSxPQUFPLEdBQUcsZ0JBQUssQ0FBVyxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUMifQ==