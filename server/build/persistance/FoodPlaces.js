"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = __importStar(require("mongoose"));
var foodPlacesSchema = new Mongoose.Schema({
    restaurantName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'restaurants name may only contain letters'
        }
    },
    shopName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'shop name may only contain letters'
        }
    },
    hotelName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'hotel name may only contain letters'
        }
    },
    shopInfo: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'shop info may only contain letters and numbers'
        }
    },
    hotelInfo: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'hotel info may only contain letters and numbers'
        }
    },
});
// Export the compiled model
exports.FoodPlaces = Mongoose.model('foodPlaces', foodPlacesSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9vZFBsYWNlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9Gb29kUGxhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLGlEQUFxQztBQUlyQyxJQUFNLGdCQUFnQixHQUFpQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQWM7SUFFcEYsY0FBYyxFQUFFO1FBQ1osSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLDJDQUEyQztTQUN2RDtLQUNKO0lBQ0QsUUFBUSxFQUFFO1FBQ04sSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLG9DQUFvQztTQUNoRDtLQUNKO0lBQ0QsU0FBUyxFQUFFO1FBQ1AsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLHFDQUFxQztTQUNqRDtLQUNKO0lBR0QsUUFBUSxFQUFFO1FBQ04sSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLGdEQUFnRDtTQUM1RDtLQUNKO0lBQ0QsU0FBUyxFQUFFO1FBQ1AsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLGlEQUFpRDtTQUM3RDtLQUNKO0NBR0osQ0FBQyxDQUFDO0FBS0gsNEJBQTRCO0FBQ2YsUUFBQSxVQUFVLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBYyxZQUFZLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyJ9