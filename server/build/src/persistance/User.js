"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
/**
 * User schema with custom validations.
 */
var userSchema = new mongoose_1.Schema({
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name may only contain letters'
        }
    },
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'last name may only contain letters'
        }
    },
    email: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/.test(value);
            },
            message: 'email should contain @ and .'
        }
    },
    passWord: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 7 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'password should contain more than seven characters'
        }
    },
});
userSchema.virtual('fullName')
    .get(function () {
    // Assemble the full name from the first and last names
    return this.firstName + ' ' + this.lastName;
}).set(function (fullName) {
    // For a simple example, the full name must separate the first name and last name with a hyphen
    // If the full name doesn't have a hyphen, throw an error
    if (!fullName.includes('-')) {
        // A proper full name for us would be 'Phillip-Fry'
        throw new Error('Full name must have a hyphen between the first and last name');
    }
    // Split should return two strings in an array, destructure assign them
    // Note: Logic here is brittle, if there are multiple dashes this will get thrown off
    var _a = fullName.split('-'), firstName = _a[0], lastName = _a[1];
    // Set the first name and last name based on pulling apart the full name
    this.firstName = firstName;
    this.lastName = lastName;
});
// Export the compiled model
exports.User = mongoose_1.model('user', userSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wZXJzaXN0YW5jZS9Vc2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBR3ZDOztHQUVHO0FBQ0gsSUFBTSxVQUFVLEdBQWtCLElBQUksaUJBQU0sQ0FBUTtJQUVoRCxTQUFTLEVBQUU7UUFDUCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFFRCxPQUFPLEVBQUUscUNBQXFDO1NBQ2pEO0tBQ0o7SUFFRCxRQUFRLEVBQUU7UUFDTixJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFFRCxPQUFPLEVBQUUsb0NBQW9DO1NBQ2hEO0tBQ0o7SUFFRCxLQUFLLEVBQUU7UUFDSCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSwwREFBMEQsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEcsQ0FBQztZQUVELE9BQU8sRUFBRSw4QkFBOEI7U0FDMUM7S0FDSjtJQUVELFFBQVEsRUFBRTtRQUNOLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFFZCxRQUFRLEVBQUU7WUFFTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUU5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUVELE9BQU8sRUFBRSxvREFBb0Q7U0FDaEU7S0FDSjtDQUdKLENBQUMsQ0FBQztBQUVILFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO0tBQ3pCLEdBQUcsQ0FBQztJQUVELHVEQUF1RDtJQUN2RCxPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7QUFFaEQsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQXVCLFFBQWdCO0lBRTFDLCtGQUErRjtJQUMvRix5REFBeUQ7SUFDekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7UUFFekIsbURBQW1EO1FBQ25ELE1BQU0sSUFBSSxLQUFLLENBQUMsOERBQThELENBQUMsQ0FBQztLQUNuRjtJQUVELHVFQUF1RTtJQUN2RSxxRkFBcUY7SUFDL0UsSUFBQSx3QkFBcUQsRUFBcEQsaUJBQVMsRUFBRSxnQkFBeUMsQ0FBQztJQUU1RCx3RUFBd0U7SUFDeEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7QUFDN0IsQ0FBQyxDQUNKLENBQUM7QUFFRiw0QkFBNEI7QUFDZixRQUFBLElBQUksR0FBRyxnQkFBSyxDQUFRLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQyJ9