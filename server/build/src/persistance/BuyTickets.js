"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = __importStar(require("mongoose"));
var ticketSchema = new Mongoose.Schema({
    startDate: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/.test(value);
            },
            message: 'start date should only contains date formate'
        }
    },
    endDate: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/.test(value);
            },
            message: 'end date should only contains date formate'
        }
    },
    numPeople: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0 && value < 7;
            },
            message: 'Number of people should only contains numbers between 0 to 7'
        }
    },
    whichPark: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 1 && /^(?![\s.]+$)[a-zA-Z\s.]*$/.test(value);
            },
            message: 'Name of the park location should only contains stings'
        }
    },
});
// Export the compiled model
exports.BuyTickets = Mongoose.model('buyTickets', ticketSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQnV5VGlja2V0cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wZXJzaXN0YW5jZS9CdXlUaWNrZXRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLGlEQUFxQztBQUlyQyxJQUFNLFlBQVksR0FBaUMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFjO0lBRWhGLFNBQVMsRUFBRTtRQUNQLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFFZCxRQUFRLEVBQUU7WUFFTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUU5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLHlEQUF5RCxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyRyxDQUFDO1lBRUQsT0FBTyxFQUFFLDhDQUE4QztTQUMxRDtLQUNKO0lBRUQsT0FBTyxFQUFFO1FBQ0wsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUkseURBQXlELENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JHLENBQUM7WUFFRCxPQUFPLEVBQUUsNENBQTRDO1NBQ3hEO0tBQ0o7SUFFRCxTQUFTLEVBQUU7UUFDUCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBRWQsUUFBUSxFQUFFO1lBRU4sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFFOUIsT0FBTyxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDbEMsQ0FBQztZQUVELE9BQU8sRUFBRSw4REFBOEQ7U0FDMUU7S0FDSjtJQUVELFNBQVMsRUFBRTtRQUNQLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFFZCxRQUFRLEVBQUU7WUFFTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUU5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2RSxDQUFDO1lBRUQsT0FBTyxFQUFFLHVEQUF1RDtTQUNuRTtLQUNKO0NBR0osQ0FBQyxDQUFDO0FBS0gsNEJBQTRCO0FBQ2YsUUFBQSxVQUFVLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBYyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUMifQ==