"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
/**
 * User schema with custom validations.
 */
var sessionSchema = new mongoose_1.Schema({
    sid: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});
// Export the compiled model
exports.Session = mongoose_1.model('session', sessionSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2Vzc2lvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wZXJzaXN0YW5jZS9TZXNzaW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBR3ZDOztHQUVHO0FBQ0gsSUFBTSxhQUFhLEdBQXFCLElBQUksaUJBQU0sQ0FBVztJQUV6RCxHQUFHLEVBQUU7UUFDRCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO0tBQ2pCO0lBRUQsS0FBSyxFQUFFO1FBQ0gsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtLQUNqQjtDQUNKLENBQUMsQ0FBQztBQUdILDRCQUE0QjtBQUNmLFFBQUEsT0FBTyxHQUFHLGdCQUFLLENBQVcsU0FBUyxFQUFFLGFBQWEsQ0FBQyxDQUFDIn0=