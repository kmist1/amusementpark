"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = __importStar(require("mongoose"));
var ridesSchema = new Mongoose.Schema({
    rideName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'rides name may only contain letters'
        }
    },
    eventName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'eventsName name may only contain letters'
        }
    },
    rideTicketPrice: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return !isNaN(value) && value > 0; //return true
            },
            message: 'rideTicketPrice  may only contain numbers from 0-9'
        }
    },
    eventTicketPrice: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return !isNaN(value) && value > 0; //return true
            },
            message: 'eventTicketPrice  may only contain numbers from 0-9'
        }
    },
    rideMonthlyPass: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return !isNaN(value) && value > 0; //return true
            },
            message: 'rideMonthlyPass  may only contain numbers from 0-9'
        }
    },
});
// Export the compiled model
exports.RidesEvents = Mongoose.model('ridesEvents', ridesSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmlkZXNFdmVudHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvcGVyc2lzdGFuY2UvUmlkZXNFdmVudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsaURBQXFDO0FBSXJDLElBQU0sV0FBVyxHQUFrQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQWU7SUFFakYsUUFBUSxFQUFFO1FBQ04sSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLHFDQUFxQztTQUNqRDtLQUNKO0lBQ0QsU0FBUyxFQUFFO1FBQ1AsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBRUQsT0FBTyxFQUFFLDBDQUEwQztTQUN0RDtLQUNKO0lBRUQsZUFBZSxFQUFFO1FBQ2IsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFBLGFBQWE7WUFDbkQsQ0FBQztZQUVELE9BQU8sRUFBRSxvREFBb0Q7U0FDaEU7S0FDSjtJQUNELGdCQUFnQixFQUFFO1FBQ2QsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUVkLFFBQVEsRUFBRTtZQUVOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBRTlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFBLGFBQWE7WUFDbkQsQ0FBQztZQUVELE9BQU8sRUFBRSxxREFBcUQ7U0FDakU7S0FDSjtJQUVELGVBQWUsRUFBRTtRQUNiLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFFZCxRQUFRLEVBQUU7WUFFTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUU5QixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQSxhQUFhO1lBQ25ELENBQUM7WUFFRCxPQUFPLEVBQUUsb0RBQW9EO1NBQ2hFO0tBQ0o7Q0FFSixDQUFDLENBQUM7QUFLSCw0QkFBNEI7QUFDZixRQUFBLFdBQVcsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFlLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQyJ9