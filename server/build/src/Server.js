"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var User_1 = require("./persistance/User");
var Session_1 = require("./persistance/Session");
var mongoose_1 = __importDefault(require("mongoose"));
var cookie_parser_1 = __importDefault(require("cookie-parser"));
var StrongParms_1 = require("./middleware/StrongParms");
var AuthenticationMiddleware_1 = require("./middleware/AuthenticationMiddleware");
var nanoid_1 = __importDefault(require("nanoid"));
var bodyParser = require('body-parser');
var app = express_1.default();
mongoose_1.default.connect('mongodb://localhost/test', { useNewUrlParser: true });
mongoose_1.default.connection.once('open', function () { return console.log("Connected to database!"); });
app.use(express_1.default.json());
app.use(bodyParser.json());
app.use(cookie_parser_1.default("I am secret for now")); // will use cypto later
/***************************************** web-Socket connection ********************************************/
var ws_1 = __importDefault(require("ws"));
var port = 5000;
var webSocketServer = new ws_1.default.Server({ port: port }); // Instantiate a new web socket server on localhost with port 5000
webSocketServer.on('connection', function (webSocketClient) {
    console.log("client has been connected to webSocket server!!");
    webSocketClient.send('Hello from the websocket server! You have successfully connected!'); // Send the connected client a message right away just to say hello
    webSocketClient.on('message', function (message) {
        console.log('received message from client: ', message);
        // The websocket server tracks all clients that connected to it, iterate through them to send a broadcast
        webSocketServer.clients.forEach(function (client) {
            // Make sure the client is still connected before trying to send a message to them
            if (client.readyState === ws_1.default.OPEN) {
                // Broadcast the clients message to all other connected clients (like a chat room)
                client.send('Broadcast message from the websocket server: ' + message);
            }
        });
        console.log('Broadcasted message to all connected clients!');
    });
});
console.log('Websocket server is up and ready for connections on port', port);
/***************************************** User Login / Session Creation ********************************************/
app.post('/login', [
    StrongParms_1.StrongParams({
        email: 'string',
        passWord: 'string',
    })
], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, email, passWord, existingUser, sid, session, error_1;
    var _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 4, , 5]);
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                email = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.email;
                passWord = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.passWord;
                console.log(strongParams);
                return [4 /*yield*/, User_1.User.findOne({ email: { $eq: email }, passWord: { $eq: passWord } })];
            case 1:
                existingUser = _d.sent();
                if (!existingUser) return [3 /*break*/, 3];
                sid = nanoid_1.default(32);
                response.cookie("userCookieName", sid, { signed: true });
                return [4 /*yield*/, Session_1.Session.create({ email: email, sid: sid })];
            case 2:
                session = _d.sent();
                //db.collection("Session").createIndex( { "createdAt": 1 }, { expireAfterSeconds: 3600 } );// for TTL
                response.send(session);
                response.locals.sid = sid;
                return [2 /*return*/, response.redirect('/homepage')];
            case 3: return [3 /*break*/, 5];
            case 4:
                error_1 = _d.sent();
                console.error('Something went wrong while creating a new user: ' + error_1.message);
                return [2 /*return*/, response.sendStatus(400)];
            case 5: return [2 /*return*/];
        }
    });
}); });
/***************************************** User Creation  ********************************************/
app.post('/user', [
    StrongParms_1.StrongParams({
        email: 'string',
        passWord: 'string',
        firstName: 'string',
        lastName: 'string'
    })
], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, email, passWord, firstName, lastName, user;
    var _a, _b, _c, _d, _e;
    return __generator(this, function (_f) {
        switch (_f.label) {
            case 0:
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                email = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.email;
                passWord = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.passWord;
                firstName = (_d = strongParams) === null || _d === void 0 ? void 0 : _d.firstName;
                lastName = (_e = strongParams) === null || _e === void 0 ? void 0 : _e.lastName;
                console.log(strongParams);
                return [4 /*yield*/, User_1.User.create({ email: email, passWord: passWord, firstName: firstName, lastName: lastName })];
            case 1:
                user = _f.sent();
                response.send(user);
                return [2 /*return*/];
        }
    });
}); });
/***************************************** User Deletion ********************************************/
app.delete('/user', AuthenticationMiddleware_1.AuthenticationMiddleware, function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var email, deletedUser;
    var _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                email = (_a = request.body) === null || _a === void 0 ? void 0 : _a.email;
                response.clearCookie("userCookieName", response.locals.sid);
                return [4 /*yield*/, User_1.User.deleteOne({
                        email: email
                    })];
            case 1:
                deletedUser = _b.sent();
                response.send(deletedUser);
                return [2 /*return*/];
        }
    });
}); });
/***************************************** Session Deletion ********************************************/
app.delete('/logout', AuthenticationMiddleware_1.AuthenticationMiddleware, function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var email, deletedSession;
    var _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                email = (_a = request.body) === null || _a === void 0 ? void 0 : _a.email;
                return [4 /*yield*/, Session_1.Session.deleteOne({
                        email: email
                    })];
            case 1:
                deletedSession = _b.sent();
                response.send(deletedSession);
                return [2 /*return*/];
        }
    });
}); });
/***************************************** Session Retrieval ********************************************/
app.post('/buyTickets', AuthenticationMiddleware_1.AuthenticationMiddleware, function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        console.log("hurray, for now just authorized that means retrieved session");
        response.send("you authorized, bruhhhh!!!!");
        return [2 /*return*/];
    });
}); });
app.listen(5000, function () { return console.log("server is up!"); });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL1NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9EQUFnRjtBQUNoRiwyQ0FBd0M7QUFDeEMsaURBQThDO0FBRTlDLHNEQUFnQztBQUNoQyxnRUFBeUM7QUFHekMsd0RBQXNEO0FBQ3RELGtGQUErRTtBQUMvRSxrREFBNEI7QUFDNUIsSUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQzFDLElBQU0sR0FBRyxHQUFnQixpQkFBTyxFQUFFLENBQUM7QUFNbkMsa0JBQVEsQ0FBQyxPQUFPLENBQUMsMEJBQTBCLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztBQUV0RSxrQkFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLGNBQU0sT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLEVBQXJDLENBQXFDLENBQUMsQ0FBQztBQUU5RSxHQUFHLENBQUMsR0FBRyxDQUFDLGlCQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQTtBQUN2QixHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO0FBQzFCLEdBQUcsQ0FBQyxHQUFHLENBQUMsdUJBQVksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUEsQ0FBQSx1QkFBdUI7QUFHbkUsOEdBQThHO0FBRzlHLDBDQUEyQjtBQUMzQixJQUFNLElBQUksR0FBVyxJQUFJLENBQUM7QUFFMUIsSUFBTSxlQUFlLEdBQW9CLElBQUksWUFBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDLGtFQUFrRTtBQUczSSxlQUFlLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFDLGVBQXlCO0lBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsaURBQWlELENBQUMsQ0FBQztJQUcvRCxlQUFlLENBQUMsSUFBSSxDQUFDLG1FQUFtRSxDQUFDLENBQUMsQ0FBQSxtRUFBbUU7SUFFN0osZUFBZSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBQyxPQUFPO1FBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFHdkQseUdBQXlHO1FBQ3pHLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBTTtZQUVuQyxrRkFBa0Y7WUFDbEYsSUFBSSxNQUFNLENBQUMsVUFBVSxLQUFLLFlBQVMsQ0FBQyxJQUFJLEVBQUU7Z0JBRXRDLGtGQUFrRjtnQkFDbEYsTUFBTSxDQUFDLElBQUksQ0FBQywrQ0FBK0MsR0FBRyxPQUFPLENBQUMsQ0FBQzthQUUxRTtRQUVMLENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO0lBQ2pFLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUM7QUFFSCxPQUFPLENBQUMsR0FBRyxDQUFDLDBEQUEwRCxFQUFFLElBQUksQ0FBQyxDQUFDO0FBRTlFLHNIQUFzSDtBQUV0SCxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFDYjtJQUNJLDBCQUFZLENBQUM7UUFDVCxLQUFLLEVBQUMsUUFBUTtRQUNkLFFBQVEsRUFBQyxRQUFRO0tBQ3BCLENBQUM7Q0FBQyxFQUNQLFVBQU8sT0FBZSxFQUFDLFFBQWlCOzs7Ozs7O2dCQUcxQixZQUFZLFNBQVMsUUFBUSxDQUFDLE1BQU0sMENBQUUsWUFBWSxDQUFDO2dCQUVuRCxLQUFLLFNBQXdCLFlBQVksMENBQUUsS0FBSyxDQUFDO2dCQUNqRCxRQUFRLFNBQXdCLFlBQVksMENBQUUsUUFBUSxDQUFDO2dCQUU3RCxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNMLHFCQUFNLFdBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBQTs7Z0JBQXpGLFlBQVksR0FBRyxTQUEwRTtxQkFDM0YsWUFBWSxFQUFaLHdCQUFZO2dCQUNOLEdBQUcsR0FBRyxnQkFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2QixRQUFRLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFDLEdBQUcsRUFBRSxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO2dCQUNyQyxxQkFBTSxpQkFBTyxDQUFDLE1BQU0sQ0FBQyxFQUFDLEtBQUssRUFBQyxLQUFLLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUE7O2dCQUFyRCxPQUFPLEdBQUcsU0FBMkM7Z0JBQzNELHFHQUFxRztnQkFDckcsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO2dCQUMxQixzQkFBTyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFDOzs7O2dCQU8xQyxPQUFPLENBQUMsS0FBSyxDQUFDLGtEQUFrRCxHQUFHLE9BQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFFbEYsc0JBQU8sUUFBUSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQzs7OztLQUV2QyxDQUFDLENBQUM7QUFFUCx1R0FBdUc7QUFFdkcsR0FBRyxDQUFDLElBQUksQ0FDSixPQUFPLEVBQ1A7SUFDSSwwQkFBWSxDQUFDO1FBQ2IsS0FBSyxFQUFDLFFBQVE7UUFDZCxRQUFRLEVBQUMsUUFBUTtRQUNqQixTQUFTLEVBQUMsUUFBUTtRQUNsQixRQUFRLEVBQUUsUUFBUTtLQUNyQixDQUFDO0NBQUMsRUFFSCxVQUFPLE9BQWdCLEVBQUUsUUFBaUI7Ozs7OztnQkFHaEMsWUFBWSxTQUFTLFFBQVEsQ0FBQyxNQUFNLDBDQUFFLFlBQVksQ0FBQztnQkFFbkQsS0FBSyxTQUF3QixZQUFZLDBDQUFFLEtBQUssQ0FBQztnQkFDakQsUUFBUSxTQUF3QixZQUFZLDBDQUFFLFFBQVEsQ0FBQztnQkFDdkQsU0FBUyxTQUF3QixZQUFZLDBDQUFFLFNBQVMsQ0FBQztnQkFDekQsUUFBUSxTQUF3QixZQUFZLDBDQUFFLFFBQVEsQ0FBQztnQkFFN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFHZCxxQkFBTSxXQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsS0FBSyxFQUFDLEtBQUssRUFBRSxRQUFRLEVBQUMsUUFBUSxFQUFFLFNBQVMsRUFBQyxTQUFTLEVBQUUsUUFBUSxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUE7O2dCQUFsRyxJQUFJLEdBQUcsU0FBMkY7Z0JBQ3hHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7S0FFMUIsQ0FBQyxDQUFDO0FBRUgsc0dBQXNHO0FBRXRHLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFDLG1EQUF3QixFQUFDLFVBQU8sT0FBZ0IsRUFBRSxRQUFpQjs7Ozs7O2dCQUc1RSxLQUFLLFNBQXVCLE9BQU8sQ0FBQyxJQUFJLDBDQUFFLEtBQUssQ0FBQztnQkFDdEQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxxQkFBTSxXQUFJLENBQUMsU0FBUyxDQUFDO3dCQUNyQyxLQUFLLEVBQUMsS0FBSztxQkFDZCxDQUFDLEVBQUE7O2dCQUZJLFdBQVcsR0FBRyxTQUVsQjtnQkFDRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDOzs7O0tBRTlCLENBQUMsQ0FBQztBQUdILHlHQUF5RztBQUV6RyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBQyxtREFBd0IsRUFBQyxVQUFPLE9BQWdCLEVBQUUsUUFBaUI7Ozs7OztnQkFFOUUsS0FBSyxTQUF1QixPQUFPLENBQUMsSUFBSSwwQ0FBRSxLQUFLLENBQUM7Z0JBRS9CLHFCQUFNLGlCQUFPLENBQUMsU0FBUyxDQUFDO3dCQUMzQyxLQUFLLEVBQUMsS0FBSztxQkFDZCxDQUFDLEVBQUE7O2dCQUZJLGNBQWMsR0FBRyxTQUVyQjtnQkFFRixRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzs7O0tBRWpDLENBQUMsQ0FBQztBQUVILDBHQUEwRztBQUUxRyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBQyxtREFBd0IsRUFBQyxVQUFNLE9BQWUsRUFBRSxRQUFpQjs7UUFFcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4REFBOEQsQ0FBQyxDQUFDO1FBQzVFLFFBQVEsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQzs7O0tBQ2hELENBQUMsQ0FBQztBQU1ILEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFDLGNBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUMifQ==